#include <iostream>
#include <stdlib.h>
#include <boost/graph/adjacency_list.hpp>
#include <random>  
const int N = 20;

using namespace std;
using namespace boost;

//Define graph type from template
typedef adjacency_list<vecS, vecS, bidirectionalS> Graph;

void add_random_edge(Graph& g)
{//Add a randomly chosen edge from lower node to higher node

    int order = num_vertices(g);
    cout << order << endl;
    graph_traits<Graph>::vertex_descriptor urand, vrand;

    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis(0, order-1);

    urand = dis(gen);
    vrand = dis(gen);
    add_edge(min(urand,vrand), max(urand,vrand),g);
}

int main(){
    //declare a graph
    Graph g;
    //Add some nodes to the graph (better way of doing this, will change)
    for(int i=0; i<10; i++){
        add_vertex(g);
    }
    //Add N random edges
    for(int j = 0; j < N; j++)
    {
        add_random_edge(g);
    }
    //Create instances of iterators to use when looking at edge lists
    Graph::vertex_iterator vertexIt, vertexEnd;
    Graph::in_edge_iterator inedgeIt, inedgeEnd;
    Graph::out_edge_iterator outedgeIt, outedgeEnd; tie(vertexIt, vertexEnd) = vertices(g);
    //Loop through vertices
    for (; vertexIt != vertexEnd; ++vertexIt) 
    {
        cout << "incoming edges for " << *vertexIt << ": ";
        //in_edges returns a pair, so use tie to untie the pair of edge iterators
        tie(inedgeIt, inedgeEnd) = in_edges(*vertexIt, g); 
        //Loop through in edges printing them
        for(; inedgeIt != inedgeEnd; ++inedgeIt) 
        { 
           cout << *inedgeIt << " "; 
        }
        cout << "\n"; 
        std::cout << "out-edges for " << *vertexIt << ":" ;
        tie(outedgeIt, outedgeEnd) = out_edges(*vertexIt, g);
        for(; outedgeIt != outedgeEnd; ++outedgeIt) 
        { 
           cout << *outedgeIt << " "; 
        }
        cout << "\n"; 
    }
    //Check for required number of edges and vertices
    std::cout << "M = ";
    std::cout << num_edges(g); 
    std::cout << " V = ";
    std::cout << num_vertices(g) << std::endl;
    return 0;
}

