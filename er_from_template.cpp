#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <random>
#include <boost/graph/adjacency_list.hpp>
#include <boost/range/irange.hpp>
#include <boost/pending/indirect_cmp.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/dag_shortest_paths.hpp>
#include <boost/config.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/properties.hpp> 
#include <boost/graph/graph_concepts.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/overloading.hpp>
#include <boost/static_assert.hpp>
#include <boost/concept/assert.hpp>
#include <string>
#include <boost/graph/filtered_graph.hpp>
using namespace std;
using namespace boost;
//Need to produce new function
const int dimensions = 2;

bool compare_time(const vector<float>& a, const vector<float>& b)
{
		return a[dimensions] < b[dimensions];
		// function that compares the temporal ordering of two nodes
}

typedef adjacency_list<vecS, vecS, bidirectionalS, 
property<vertex_distance_t, int>, property<edge_weight_t, int> > Graph;

template <class ComponentsMap>
/* Here it's creating a template for a class which takes different propery map
types. The ComponentsMap is a template for a type. This is so the function can 
work with multiple types without having to redefine the whole function.
These concepts are covered in the cpp documentation 
in 'Classes II'. It was a bit of a mind-fudge before reading that.*/
class causal_chain_recorder : public dfs_visitor<>
//The class is inherited from the boost ^ dfs_visitor class
{
	typedef typename property_traits<ComponentsMap>::value_type comp_type;
  	//this defines a type from a template. As in the type of the defined object
  	//is type. This is so that when labeling the components, it knows what type
  	//to use for the labels. 
public:
  	std::vector<long>* m_cone_dist;
  	causal_chain_recorder(ComponentsMap c,
						  comp_type& c_count, std::vector<long>* cone_dist)
		: m_component(c), m_count(c_count), m_cone_dist(cone_dist) {}

 	template <class Vertex, class Graph>
  	void start_vertex(Vertex, Graph&) 
  	{
		if (m_count == (std::numeric_limits<comp_type>::max)())
		  m_count = 0; // start counting components at zero
		else ++m_count;
		m_cone_dist->push_back(0);
	}

  	template <class Vertex, class Graph>
  	void discover_vertex(Vertex u, Graph&) 
  	{
		put(m_component, u, m_count);
		(*m_cone_dist)[m_count]++;
	}
protected:
  	ComponentsMap m_component;
  	comp_type& m_count;
};

template <class Graph, class ComponentMap>
inline typename property_traits<ComponentMap>::value_type
light_cone_dist(const Graph& g, ComponentMap c, std::vector<long>& cone_dist
				   BOOST_GRAPH_ENABLE_IF_MODELS_PARM(Graph, vertex_list_graph_tag))
{ 
	if (num_vertices(g) == 0) return 0;

	typedef typename graph_traits<Graph>::vertex_descriptor Vertex;
	BOOST_CONCEPT_ASSERT(( WritablePropertyMapConcept<ComponentMap, Vertex> ));
	// typedef typename boost::graph_traits<Graph>::directed_category directed;
	// BOOST_STATIC_ASSERT((boost::is_same<directed, undirected_tag>::value));

	typedef typename property_traits<ComponentMap>::value_type comp_type;
	// c_count initialized to "nil" (with nil represented by (max)())
	comp_type c_count((std::numeric_limits<comp_type>::max)());
	causal_chain_recorder<ComponentMap> vis(c, c_count, &cone_dist);
	depth_first_search(g, visitor(vis));
	return c_count + 1;
}

void add_random_edge(Graph& g)
{
	//Add a randomly chosen edge from lower node to higher node
	unsigned long order = num_vertices(g);
	graph_traits<Graph>::vertex_descriptor urand, vrand;


	std::random_device rd;  //Will be used to obtain a seed for the random number engine
	std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
	std::uniform_int_distribution<graph_traits<Graph>::vertex_descriptor> dis(0, num_vertices(g)-1);
	graph_traits<Graph>::vertex_descriptor maxrand,minrand;
	do{
	  urand = dis(gen);
	  do{
		vrand = dis(gen);
	  } 
	  while(urand == vrand);
	  maxrand = max(urand, vrand);
	  minrand = min(urand, vrand);}
	while(boost::edge(minrand,maxrand,g).second);//contains maxrand && maxrand != minrand
	add_edge(minrand, maxrand, 1, g);
}

void add_random_edge(Graph& g, Graph& neg_graph)
{
	//Add a randomly chosen edge from lower node to higher node
	unsigned long order = num_vertices(g);
	graph_traits<Graph>::vertex_descriptor urand, vrand;


	std::random_device rd;  //Will be used to obtain a seed for the random number engine
	std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
	std::uniform_int_distribution<graph_traits<Graph>::vertex_descriptor> dis(0, num_vertices(g)-1);
	graph_traits<Graph>::vertex_descriptor maxrand,minrand;
	do
	{
	  	urand = dis(gen);
	  	do
	  	{
			vrand = dis(gen);
	  	} 
	  	while(urand == vrand);
	  	maxrand = max(urand, vrand);
	  	minrand = min(urand, vrand);}
	while(boost::edge(minrand,maxrand,g).second);//contains maxrand && maxrand != minrand
	add_edge(minrand, maxrand, 1, g);
	add_edge(minrand, maxrand, -1, neg_graph);
}

//void output_graph(&Graph g, string name, string location){
void output_edges(Graph& g)
{
	graph_traits<Graph>::edge_iterator ei, ei_end;
	for (tie(ei, ei_end) = edges(g); ei != ei_end; ++ei)
	{
	  	cout << *ei << endl;
	}
}

int longest_path(Graph& neg_graph, graph_traits<Graph>::vertex_descriptor s)
{
	property_map<Graph, vertex_distance_t>::type d_map = get(vertex_distance, neg_graph);
	vector<graph_traits<Graph>::vertex_descriptor> p(num_vertices(neg_graph));
	vector<graph_traits<Graph>::vertex_descriptor> d(num_vertices(neg_graph));
	dag_shortest_paths(neg_graph, s, distance_map(d_map));
	graph_traits<Graph>::vertex_iterator vi , vi_end;
	int longest = 0;
	for (tie(vi, vi_end) = vertices(neg_graph); vi != vi_end; ++vi)
	{
		if (d_map[*vi] < longest)
		{
		  longest = d_map[*vi];
		}
	}
	return -longest;
}



int main()
{
	//Set up the timestamping
	auto t = std::time(nullptr);
	auto time = *std::localtime(&t);

	string name = "erstatsmultiple.csv";//std::put_time(&time, "stats_%d-%m-%y_%H:%M:%S.csv");
	
	//Check if file exists already
	ifstream f(name.c_str());
	bool empty = true;
	if (f.good()) empty = false;
	f.close();
	
	//If file already exists, don't write in the headers when opened
	ofstream data;
	data.open (name, std::ios_base::app);
	if(empty) data << "vertices,edges,size,lp,lsp,asp,avg_deg\n";
	
//INITIALISE GRAPH
	//Make the graph and negative graph
	for(int running=0; running<1; running++)
	{
		for(int system_num=1; system_num<2; system_num++)
		{
			int num_nodes, steps, num_edges, edge_curr;
			num_nodes = 256*pow(2,system_num);
			steps =100;
			num_edges = 100;
			float x = 1.018;
			if(pow(x,(steps+1))*num_edges > num_nodes*(num_nodes-1)/2)
			{
				cout << "too many edges" << endl;
				return -1;
			}
			edge_curr = num_edges;
			std::random_device rd; 
    		std::mt19937 gen(rd()); 
    		std::uniform_real_distribution<> dis(0, 1);

			// make N x (D+1) vector for position of each node and final element is the nodes time sqrd
			Graph g(num_nodes); 
			Graph neg_graph(num_nodes); // make graph for longest path
			vector<pair<int,int>> edge_pool;
			for(int j=0; j<num_nodes; j++)
			{
				for(int k=0; k<j; k++)
				{
					pair<int,int> p{k,j};
					edge_pool.push_back(p);
				}
			}
			random_shuffle(edge_pool.begin(),edge_pool.end());
			for(int step = 0; step<steps; step++)
			{
				for(int edge_number=0; edge_number<num_edges; edge_number++)
				{	
					int u,v;
					tie(u,v) = edge_pool.back();
					edge_pool.pop_back();
					add_edge(u,v,1,g);
					add_edge(u,v,-1,neg_graph);
				}

		  		//Initialise property maps for the algorithms
		  		vector<unsigned long> component(num_vertices(g));
		  		vector<long> cone_dist;
		  
		  /*
		  Pass cone_dist and component into light_cone_dist. This uses
		  the boost dfs to find the sizes of all lightcones. cone_dist will contain
		  the sizes of each light cone. Component is redundant but is required as a
		  property map for the boost function to work properly
		  */
				//MAX LIGHTCONE SIZE
				int comp_num = light_cone_dist(g, make_iterator_property_map(component.begin(), get(vertex_index, g)),cone_dist);
				long max_cone = *max_element(cone_dist.begin(),cone_dist.end());

			//LONGEST PATH
			  	//Identify sources of light cones and find longest path
			  	long lpath=0;
			  	for(int i = 1; i < comp_num; i++)
			  	{
					int loc = find(component.begin(), component.end(), i) - component.begin();
					int next_lpath = longest_path(neg_graph, loc);
					if(next_lpath > lpath) lpath = next_lpath;
			  	}
			//SHORTEST PATHS

		  		int lsp_mean_method = 0;
		  		int cumul_sp_mean_meth = 0;
		  		int num_conn_nodes_mean_meth=0;
		  		int mean_meth_loops = 0;
		  		float averaging_criteria = 0.0001;
		  		int num_averaging_over = 10;
		  		std::uniform_int_distribution<> rnd(0,num_nodes-1);
		  		vector<float> last_five_means (num_averaging_over,0);
		  		for(int i =0; i<num_averaging_over; i++)
		  		{
					mean_meth_loops+=1;
					int s= rnd(gen); // note - I am choosing not to prevent same vertex being selected twice
					property_map<Graph, vertex_distance_t>::type d_map = get(vertex_distance, g);
					dag_shortest_paths(g,s,distance_map(d_map));
					graph_traits<Graph>::vertex_iterator vi, vi_end;
					for(tie(vi, vi_end) = vertices(g); vi!=vi_end; ++vi)
					{
			  			if(d_map[*vi] <num_nodes)
			  			{
							// cout << s << ", " << *vi << ": " << d_map[*vi] << endl; 
							cumul_sp_mean_meth+=d_map[*vi];
							num_conn_nodes_mean_meth += 1;
							if(d_map[*vi]>lsp_mean_method)
							{
				  				lsp_mean_method = d_map[*vi];
							}
			  			}
					}
					float tmp_avg = float(cumul_sp_mean_meth)/num_conn_nodes_mean_meth;
					last_five_means[i]=tmp_avg;
		  		}
		  		bool keep_mean_meth_loop_going = true;
		  		float tmp_avg = float(cumul_sp_mean_meth)/num_conn_nodes_mean_meth;
		  		float fluctuations = 0;
		  		for(int j =0; j<5; j++)
		  		{
					fluctuations += pow((last_five_means[j]-tmp_avg),2)/pow(tmp_avg,2);
		  		}
		  		if(fluctuations<averaging_criteria)
		  		{
					keep_mean_meth_loop_going = false;
				}
		  		while(keep_mean_meth_loop_going)
		  		{
					mean_meth_loops+=1;
					int s= rnd(gen); // note - I am choosing not to prevent same vertex being selected twice
					property_map<Graph, vertex_distance_t>::type d_map = get(vertex_distance, g);
					dag_shortest_paths(g,s,distance_map(d_map));
					graph_traits<Graph>::vertex_iterator vi, vi_end;
					for(tie(vi, vi_end) = vertices(g); vi!=vi_end; ++vi)
					{
			  			if(d_map[*vi] <num_nodes)
			  			{
							cumul_sp_mean_meth+=d_map[*vi];
							num_conn_nodes_mean_meth += 1;
							if(d_map[*vi]>lsp_mean_method)
							{
				  				lsp_mean_method = d_map[*vi];
							}
			  			}
					}
					for(int i = 0; i<num_averaging_over-1; i++)
					{
			  			last_five_means[i]=last_five_means[i+1];
					}
					float tmp_avg = float(cumul_sp_mean_meth)/num_conn_nodes_mean_meth;
					last_five_means[num_averaging_over-1]= tmp_avg;
					float fluctuations = 0;
					for(int j =0; j<num_averaging_over; j++)
					{
			  			fluctuations += pow((last_five_means[j]-tmp_avg),2)/pow(tmp_avg,2);
					}
					if(fluctuations<averaging_criteria)
					{
			  			keep_mean_meth_loop_going = false;
			  		}
					if(mean_meth_loops==num_nodes)
					{
			  			break;
			  		}
		  		}


		  		// calculate average degree
		  		float avg_deg = 0.0;
		  		for(int n =0; n<num_nodes; n++)
		  		{
					avg_deg += float(degree(n,g))/num_nodes;
				}
	//OUTPUT DATA
		  		data << num_nodes;
		  		data << ",";
		  		data << edge_curr;
		  		data << ",";
		  		data << max_cone;
		  		data << ",";
		  		data << lpath;
		  		data << ",";
		  		data << lsp_mean_method;
		  		data << ",";
		  		data << last_five_means[num_averaging_over-1];
		  		data << ", ";
		  		data << avg_deg << endl;
		  		edge_curr += num_edges;
		  		num_edges=ceil(float(num_edges)*x);
			}
	  	}
	}  
  	data.close();
  	return 0;
}
