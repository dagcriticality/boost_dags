from subprocess import Popen, PIPE
import numpy as np
import matplotlib.pyplot as plt

def connected_component(n,m,trials,steps):
    """Generate a random ER dag with n vertices and m edges"""

    p = Popen(['./ER_GCCavgtest'], shell=True, stdout=PIPE, stdin=PIPE)
    for i in [n,m,trials,steps]:
        param = str(i) + '\n'
        param = bytes(param, 'UTF-8')
        p.stdin.write(param)
        p.stdin.flush()
        [(i,int(p.stdout.readline().strip())) for i in range(steps)]
    LCC = p.stdout.readline().strip()
    return int(LCC.decode('UTF-8'))

def plot_list(n, trials, ranges = range(3,20,2)):
    q = [connected_component(n,i*n,trials)/n for i in ranges]
    plt.plot(ranges, q)