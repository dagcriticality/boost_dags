#include <iostream>
#include <stdlib.h>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/exterior_property.hpp>
#include <boost/graph/floyd_warshall_shortest.hpp>
#include <random>

using namespace std;
using namespace boost;

typedef double t_weight;
typedef property<edge_weight_t, t_weight> EdgeWeightProperty;
typedef adjacency_list<vecS, vecS, directedS, no_property, EdgeWeightProperty> Graph;

typedef property_map<Graph, edge_weight_t>::type WeightMap;
typedef exterior_vertex_property<Graph, t_weight> DistanceProperty;
typedef DistanceProperty::matrix_type DistanceMatrix;
typedef DistanceProperty::matrix_map_type DistanceMatrixMap;


void add_random_edge(Graph& g)
{//Add a randomly chosen edge from lower node to higher node

    int order = num_vertices(g);
    graph_traits<Graph>::vertex_descriptor urand, vrand;


    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis(0, order-1);

    urand = dis(gen);
    vrand = dis(gen);
    while(vrand == urand){
        vrand = dis(gen);
    }
    add_edge(min(urand,vrand), max(urand,vrand),EdgeWeightProperty(1),g);
}

int main(){
    long int num_nodes, num_edges;
    cin >> num_nodes >> num_edges;
    Graph g;
    //Add some nodes to the graph (better way of doing this, will change)
    for(int i=0; i < num_nodes; i++){
        add_vertex(g);
    }
    //Add N random edges
    for(int j = 0; j < num_edges; j++)
    {
        add_random_edge(g);
    }

    WeightMap weight_pmap = get(edge_weight, g);

    DistanceMatrix distances(num_vertices(g));
    DistanceMatrixMap dm(distances, g);


    bool valid = floyd_warshall_all_pairs_shortest_paths(g,dm, weight_map(weight_pmap));

    int longest_shortest_dist = 0;
    for (size_t i = 0; i<num_vertices(g); ++i){
        for(size_t j = 0; j<num_vertices(g); ++j){
            if(distances[i][j] != numeric_limits<t_weight>::max()){
                if(distances[i][j] > longest_shortest_dist){
                    longest_shortest_dist = distances[i][j];
                }
            }
        }
    }
    cout <<longest_shortest_dist<<endl;

    return 0;
 }