#include <iostream>
#include <stdlib.h>
#include <boost/graph/adjacency_list.hpp>
#include <random>


using namespace std;
using namespace boost;


struct vertex_properties
    {
    float xpos;
    float ypos;
};

typedef double t_weight;
typedef property<edge_weight_t, t_weight> EdgeWeightProperty;
typedef adjacency_list<vecS, vecS, directedS, vertex_properties, EdgeWeightProperty> Graph;


int main(){

    int num_vertices = 100;
    float radius = 0.2;

    // make N x 3 vector with each nodes xpos, ypos, time_sqrd

    vector<vector<float>> node_list (num_vertices,vector<float>(3)) ;
    for(int i=0; i<num_vertices; i++){ // believe using indexing is quicker here than using iterators
        std::random_device rd; 
        std::mt19937 gen(rd()); 
        std::uniform_int_distribution<> dis(0, 1E5);
        float xrand = dis(gen)/1E5;
        float yrand = dis(gen)/1E5;
        node_list[i][0]= xrand;
        node_list[i][1]=yrand;
        float time_sqrd = (xrand*xrand)+(yrand*yrand);
        node_list[i][2]=time_sqrd;
    }

    // order vector in terms of time value

    sort(node_list.begin(), node_list.end(),[](const std::vector<float>& a, const std::vector<float>& b) {
            return a[2] < b[2];
        });

    // add all vertices to graph and add edges if within radius

    Graph g;
    for(int j=0; j<num_vertices; j++){
        Graph::vertex_descriptor v = add_vertex(g);
        g[v].xpos=node_list[j][0];
        g[v].ypos=node_list[j][1];
        for(int k=0; k<j; k++){
            float distx=node_list[j][0]-node_list[k][0];
            float disty=node_list[j][1]-node_list[k][1];
            if(abs(distx)<radius || abs(disty)<radius){
                graph_traits<Graph>::vertex_descriptor u;
                u=vertex(k,g);
                pair<adjacency_list<>::edge_descriptor, bool> q;
                q=add_edge(u,v,EdgeWeightProperty(1), g);
            }
        }
    }

    
    return 0;
}

