#include <iostream>
#include <stdlib.h>
#include <boost/graph/adjacency_list.hpp>
#include <boost/range/irange.hpp>
#include <boost/pending/indirect_cmp.hpp>
#include <random>  
#include <boost/config.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/graph_concepts.hpp>
#include <boost/graph/overloading.hpp>
#include <boost/static_assert.hpp>
#include <boost/concept/assert.hpp>
using namespace std;
using namespace boost;
const int N = 10;


template <class ComponentsMap>
/* Here it's creating a template for a class which takes different propery map
types. The ComponentsMap is a template for a type. This is so the function can 
work with multiple types without having to redefine the whole function.
These concepts are covered in the cpp documentation 
in 'Classes II'. It was a bit of a mind-fudge before reading that.*/
class components_recorder : public dfs_visitor<>
//The class is inherited from the boost ^ dfs_visitor class
{
  typedef typename property_traits<ComponentsMap>::value_type comp_type;
  //this defines a type from a template. As in the type of the defined object
  //is type. This is so that when labeling the components, it knows what type
  //to use for the labels. 
public:
  components_recorder(ComponentsMap c, 
                      comp_type& c_count)
    : m_component(c), m_count(c_count) {}

  template <class Vertex, class Graph>
  void start_vertex(Vertex, Graph&) {
    if (m_count == (std::numeric_limits<comp_type>::max)())
      m_count = 0; // start counting components at zero
    else
      ++m_count;
  }
  template <class Vertex, class Graph>
  void discover_vertex(Vertex u, Graph&) {
    put(m_component, u, m_count);
  }
protected:
  ComponentsMap m_component;
  comp_type& m_count;
};

typedef adjacency_list<vecS, vecS, bidirectionalS> Graph;

template <class Graph, class ComponentMap>
inline typename property_traits<ComponentMap>::value_type
connected_components(const Graph& g, ComponentMap c
                   BOOST_GRAPH_ENABLE_IF_MODELS_PARM(Graph, vertex_list_graph_tag))
{
if (num_vertices(g) == 0) return 0;

typedef typename graph_traits<Graph>::vertex_descriptor Vertex;
BOOST_CONCEPT_ASSERT(( WritablePropertyMapConcept<ComponentMap, Vertex> ));
// typedef typename boost::graph_traits<Graph>::directed_category directed;
// BOOST_STATIC_ASSERT((boost::is_same<directed, undirected_tag>::value));

typedef typename property_traits<ComponentMap>::value_type comp_type;
// c_count initialized to "nil" (with nil represented by (max)())
comp_type c_count((std::numeric_limits<comp_type>::max)());
components_recorder<ComponentMap> vis(c, c_count);
depth_first_search(g, visitor(vis));
return c_count + 1;
}

void add_random_edge(Graph& g)
{//Add a randomly chosen edge from lower node to higher node

    int order = num_vertices(g);
    graph_traits<Graph>::vertex_descriptor urand, vrand;


    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis(0, order-1);

    urand = dis(gen);
    vrand = dis(gen);
    while(vrand == urand){
        vrand = dis(gen);
    }
    add_edge(min(urand,vrand), max(urand,vrand),g);
}

int main(){
    long int num_nodes, num_edges;
    cin >> num_nodes >> num_edges;
    Graph g;
    //Add some nodes to the graph (better way of doing this, will change)
    for(int i=0; i < num_nodes; i++){
        add_vertex(g);
    }
    //Add N random edges
    for(int j = 0; j < num_edges; j++)
    {
        add_random_edge(g);
    }
    vector<int> component(num_vertices(g));
    connected_components(g, make_iterator_property_map(component.begin(), get(vertex_index, g)));
    long ncc = -1;
    for(int i = 0; i < num_vertices(g); i++)
    {
        if(component[i] > ncc)
        {
          ncc = component[i];
        }
    }
    vector<long> comps(ncc+1);
    for(int i = 0; i < num_vertices(g); i++)
    {
        comps[component[i]]++;
    }
    long maxcomp;
    for(int i = 0; i < comps.size(); i++)
    {
        if(comps[i] > maxcomp)
        {
          maxcomp = comps[i];
        }
    }
    cout << maxcomp << endl;
  return 0;
}