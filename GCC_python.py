from subprocess import Popen, PIPE

def connected_component(n,m):
    """Generate a random ER dag with n vertices and m edges"""

    p = Popen(['./ER_GCC'], shell=True, stdout=PIPE, stdin=PIPE)
    for i in [n,m]:
        param = str(i) + '\n'
        param = bytes(param, 'UTF-8')
        p.stdin.write(param)
        p.stdin.flush()
    LCC = p.stdout.readline().strip()
    return int(LCC.decode('UTF-8'))