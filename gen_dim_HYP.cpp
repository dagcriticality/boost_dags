#include <iostream>
#include <stdlib.h>
#include <boost/graph/adjacency_list.hpp>
#include <random>


using namespace std;
using namespace boost;

const int dimensions = 10;// dimensions = spacial dimensions = D - NOTE DIFFERENT TO BOX




// EdgeWeight stuff is just set so all edges can have weights = 1
typedef double t_weight;
typedef property<edge_weight_t, t_weight> EdgeWeightProperty;
typedef adjacency_list<vecS, vecS, directedS, no_property, EdgeWeightProperty> Graph;

bool compare(const vector<float>& a, const vector<float>& b){
        return a[dimensions] < b[dimensions];
    }


int main(){

    int Num_Vert;
    float radius;
    cin >> Num_Vert >> radius;

    // make N x (D+1) vector for position of each node and final element is the nodes time sqrd

    vector< vector<float> > node_list (Num_Vert,vector<float>(dimensions +1)) ;
    for(int i=0; i<Num_Vert; i++){ // believe using indexing is quicker here than using iterators
        std::random_device rd; 
        std::mt19937 gen(rd()); 
        std::uniform_int_distribution<> dis(0, 1E5);
        float time_sqrd = 0.0;
        for(int d=0; d<dimensions; d++){
            float pos_rand = dis(gen)/1E5;
            time_sqrd += (pos_rand*pos_rand);
            node_list[i][d] = pos_rand;
        }
        node_list[i][dimensions]=time_sqrd;
    }

    // order vector in terms of time value

    sort(node_list.begin(), node_list.end(),compare);
    // add all vertices to graph and add edges if within radius

    Graph g;
    for(int j=0; j<Num_Vert; j++){
        Graph::vertex_descriptor v = add_vertex(g);
        for(int k=0; k<j; k++){
            float change_t2=(node_list[j][dimensions]*node_list[j][dimensions])-(node_list[k][dimensions]*node_list[k][dimensions]);
            float change_r2=0.0;
            for(int d=0; d<dimensions-1; d++){
                change_r2+=(node_list[j][d]*node_list[j][d])-(node_list[k][d]*node_list[k][d]);
            }
            float change_tau2=change_t2-change_r2;
            if(0<change_tau2<radius){
                graph_traits<Graph>::vertex_descriptor u;
                u=vertex(k,g);
                pair<adjacency_list<>::edge_descriptor, bool> q;
                q=add_edge(u,v,EdgeWeightProperty(1), g);
            }
            
        }
    }
    cout << "Done" << endl;
    
    return 0;
}

