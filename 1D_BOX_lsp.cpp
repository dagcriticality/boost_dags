#include <iostream>
#include <stdlib.h>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/exterior_property.hpp>
#include <boost/graph/floyd_warshall_shortest.hpp>
#include <random>

using namespace std;
using namespace boost;


struct vertex_properties
    {
    float xpos;
    float ypos;
    };

typedef double t_weight;
typedef property<edge_weight_t, t_weight> EdgeWeightProperty;

typedef adjacency_list<vecS, vecS, directedS, vertex_properties, EdgeWeightProperty> Graph;
// look up vecS compared to linked list sort of thing

typedef property_map<Graph, edge_weight_t>::type WeightMap;
typedef exterior_vertex_property<Graph, t_weight> DistanceProperty;
typedef DistanceProperty::matrix_type DistanceMatrix;
typedef DistanceProperty::matrix_map_type DistanceMatrixMap;


bool compare(const vector<float>& a, const vector<float>& b){
        return a[2] < b[2];
    }

int main(){

    float Rad;
    int Num_Vert;
    cin >> Num_Vert >> Rad;


// make N x 3 vector with each nodes xpos, ypos, time_sqrd

    vector< vector<float> > node_list (Num_Vert,vector<float>(3)) ;
    for(int i=0; i<Num_Vert; i++){ // believe using indexing is quicker here than using iterators
        std::random_device rd; 
        std::mt19937 gen(rd()); 
        std::uniform_int_distribution<> dis(0, 1E5);
        float xrand = dis(gen)/1E5;
        float yrand = dis(gen)/1E5;
        node_list[i][0]= xrand;
        node_list[i][1]=yrand;
        float time_sqrd = (xrand*xrand)+(yrand*yrand);
        node_list[i][2]=time_sqrd;
    }

    // order vector in terms of time value
    

    sort(node_list.begin(), node_list.end(),compare);

    // add all vertices to graph and add edges if within radius


    Graph g;
    for(int j=0; j<Num_Vert; j++){
        Graph::vertex_descriptor v = add_vertex(g);
        g[v].xpos=node_list[j][0];
        g[v].ypos=node_list[j][1];
        for(int k=0; k<j; k++){
            float distx=node_list[j][0]-node_list[k][0];
            float disty=node_list[j][1]-node_list[k][1];
            if(abs(distx)<Rad || abs(disty)<Rad){
                graph_traits<Graph>::vertex_descriptor u;
                u=vertex(k,g);
                pair<adjacency_list<>::edge_descriptor, bool> q;
                q=add_edge(u,v,EdgeWeightProperty(1), g);
            }
        }
    }

   

    WeightMap weight_pmap = get(edge_weight, g);

    DistanceMatrix distances(num_vertices(g));
    DistanceMatrixMap dm(distances, g);


    bool valid = floyd_warshall_all_pairs_shortest_paths(g,dm, weight_map(weight_pmap));

    int longest_shortest_dist = 0;
    for (size_t i = 0; i<num_vertices(g); ++i){
        for(size_t j = 0; j<num_vertices(g); ++j){
            if(distances[i][j] != numeric_limits<t_weight>::max()){
                if(distances[i][j] > longest_shortest_dist){
                    longest_shortest_dist = distances[i][j];
                }
            }
        }
    }
    cout << longest_shortest_dist << endl;
    
    return 0;
}

