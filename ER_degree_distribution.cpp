#include <iostream>
#include <stdlib.h>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/exterior_property.hpp>
#include <random>

using namespace std;
using namespace boost;

typedef double t_weight;
typedef property<edge_weight_t, t_weight> EdgeWeightProperty;
typedef adjacency_list<vecS, vecS, directedS, no_property, EdgeWeightProperty> Graph;

typedef property_map<Graph, edge_weight_t>::type WeightMap;
typedef exterior_vertex_property<Graph, t_weight> DistanceProperty;
typedef DistanceProperty::matrix_type DistanceMatrix;
typedef DistanceProperty::matrix_map_type DistanceMatrixMap;


void add_random_edge(Graph& g)
{//Add a randomly chosen edge from lower node to higher node

    int order = num_vertices(g);
    graph_traits<Graph>::vertex_descriptor urand, vrand;


    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis(0, order-1);

    urand = dis(gen);
    vrand = dis(gen);
    while(vrand == urand){
        vrand = dis(gen);
    }
    add_edge(min(urand,vrand), max(urand,vrand),EdgeWeightProperty(1),g);
}

int main(){
    long int num_nodes, num_edges;
    cin >> num_nodes >> num_edges;

    int ensemble_num = 10000;
    vector<float> results (num_nodes, 0.0);

    for(int run=0; run<ensemble_num; run++){
    Graph g;
    //Add some nodes to the graph (better way of doing this, will change)
    for(int i=0; i < num_nodes; i++){
        add_vertex(g);
    }
    //Add N random edges
    for(int j = 0; j < num_edges; j++)
    {
        add_random_edge(g);
    }



        for(int i=0; i<num_nodes; i++){
            int temp = out_degree(i,g);
            float ans = float(temp)/float(ensemble_num);
            results[i]+=ans;
        }
    }

    for(int j=0; j<num_nodes; j++){
        cout << results[j] << endl;
    }

    return 0;
}
