#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <boost/graph/adjacency_list.hpp>
#include <boost/range/irange.hpp>
#include <boost/pending/indirect_cmp.hpp>
#include <boost/graph/graph_traits.hpp>
#include <random>  
#include <boost/graph/dag_shortest_paths.hpp>
#include <boost/config.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/properties.hpp> 
#include <boost/graph/graph_concepts.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/overloading.hpp>
#include <boost/static_assert.hpp>
#include <boost/concept/assert.hpp>
#include <string>
#include <boost/graph/filtered_graph.hpp>
using namespace std;
using namespace boost;
//Need to produce new function
const int dimensions = 1;

bool compare_time(const vector<float>& a, const vector<float>& b)
{
		return a[dimensions] < b[dimensions];
		// function that compares the temporal ordering of two nodes
}

typedef adjacency_list<vecS, vecS, bidirectionalS, 
property<vertex_distance_t, int>, property<edge_weight_t, int> > Graph;

template <class ComponentsMap>
/* Here it's creating a template for a class which takes different propery map
types. The ComponentsMap is a template for a type. This is so the function can 
work with multiple types without having to redefine the whole function.
These concepts are covered in the cpp documentation 
in 'Classes II'. It was a bit of a mind-fudge before reading that.*/
class causal_chain_recorder : public dfs_visitor<>
//The class is inherited from the boost ^ dfs_visitor class
{
	typedef typename property_traits<ComponentsMap>::value_type comp_type;
  	//this defines a type from a template. As in the type of the defined object
  	//is type. This is so that when labeling the components, it knows what type
  	//to use for the labels. 
public:
  	std::vector<long>* m_cone_dist;
  	causal_chain_recorder(ComponentsMap c,
						  comp_type& c_count, std::vector<long>* cone_dist)
		: m_component(c), m_count(c_count), m_cone_dist(cone_dist) {}

 	template <class Vertex, class Graph>
  	void start_vertex(Vertex, Graph&) 
  	{
		if (m_count == (std::numeric_limits<comp_type>::max)())
		  m_count = 0; // start counting components at zero
		else ++m_count;
		m_cone_dist->push_back(0);
	}

  	template <class Vertex, class Graph>
  	void discover_vertex(Vertex u, Graph&) 
  	{
		put(m_component, u, m_count);
		(*m_cone_dist)[m_count]++;
	}
protected:
  	ComponentsMap m_component;
  	comp_type& m_count;
};

template <class Graph, class ComponentMap>
inline typename property_traits<ComponentMap>::value_type
light_cone_dist(const Graph& g, ComponentMap c, std::vector<long>& cone_dist
				   BOOST_GRAPH_ENABLE_IF_MODELS_PARM(Graph, vertex_list_graph_tag))
{ 
	if (num_vertices(g) == 0) return 0;

	typedef typename graph_traits<Graph>::vertex_descriptor Vertex;
	BOOST_CONCEPT_ASSERT(( WritablePropertyMapConcept<ComponentMap, Vertex> ));
	// typedef typename boost::graph_traits<Graph>::directed_category directed;
	// BOOST_STATIC_ASSERT((boost::is_same<directed, undirected_tag>::value));

	typedef typename property_traits<ComponentMap>::value_type comp_type;
	// c_count initialized to "nil" (with nil represented by (max)())
	comp_type c_count((std::numeric_limits<comp_type>::max)());
	causal_chain_recorder<ComponentMap> vis(c, c_count, &cone_dist);
	depth_first_search(g, visitor(vis));
	return c_count + 1;
}

void add_random_edge(Graph& g)
{
	//Add a randomly chosen edge from lower node to higher node
	unsigned long order = num_vertices(g);
	graph_traits<Graph>::vertex_descriptor urand, vrand;


	std::random_device rd;  //Will be used to obtain a seed for the random number engine
	std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
	std::uniform_int_distribution<graph_traits<Graph>::vertex_descriptor> dis(0, num_vertices(g)-1);
	graph_traits<Graph>::vertex_descriptor maxrand,minrand;
	do{
	  urand = dis(gen);
	  do{
		vrand = dis(gen);
	  } 
	  while(urand == vrand);
	  maxrand = max(urand, vrand);
	  minrand = min(urand, vrand);}
	while(boost::edge(minrand,maxrand,g).second);//contains maxrand && maxrand != minrand
	add_edge(minrand, maxrand, 1, g);
}

void add_random_edge(Graph& g, Graph& neg_graph)
{
	//Add a randomly chosen edge from lower node to higher node
	unsigned long order = num_vertices(g);
	graph_traits<Graph>::vertex_descriptor urand, vrand;


	std::random_device rd;  //Will be used to obtain a seed for the random number engine
	std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
	std::uniform_int_distribution<graph_traits<Graph>::vertex_descriptor> dis(0, num_vertices(g)-1);
	graph_traits<Graph>::vertex_descriptor maxrand,minrand;
	do
	{
	  	urand = dis(gen);
	  	do
	  	{
			vrand = dis(gen);
	  	} 
	  	while(urand == vrand);
	  	maxrand = max(urand, vrand);
	  	minrand = min(urand, vrand);}
	while(boost::edge(minrand,maxrand,g).second);//contains maxrand && maxrand != minrand
	add_edge(minrand, maxrand, 1, g);
	add_edge(minrand, maxrand, -1, neg_graph);
}

//void output_graph(&Graph g, string name, string location){
void output_edges(Graph& g)
{
	graph_traits<Graph>::edge_iterator ei, ei_end;
	for (tie(ei, ei_end) = edges(g); ei != ei_end; ++ei)
	{
	  	cout << *ei << endl;
	}
}

int longest_path(Graph& neg_graph, graph_traits<Graph>::vertex_descriptor s)
{
	property_map<Graph, vertex_distance_t>::type d_map = get(vertex_distance, neg_graph);
	vector<graph_traits<Graph>::vertex_descriptor> p(num_vertices(neg_graph));
	vector<graph_traits<Graph>::vertex_descriptor> d(num_vertices(neg_graph));
	dag_shortest_paths(neg_graph, s, distance_map(d_map));
	graph_traits<Graph>::vertex_iterator vi , vi_end;
	int longest = 0;
	for (tie(vi, vi_end) = vertices(neg_graph); vi != vi_end; ++vi)
	{
		if (d_map[*vi] < longest)
		{
		  longest = d_map[*vi];
		}
	}
	return -longest;
}



int main()
{
	//Set up the timestamping
	auto t = std::time(nullptr);
	auto time = *std::localtime(&t);

	string name = "hyp_path_dist.csv";//std::put_time(&time, "stats_%d-%m-%y_%H:%M:%S.csv");
	
	//Check if file exists already
	ifstream f(name.c_str());
	bool empty = true;
	if (f.good()) empty = false;
	f.close();
	
	//If file already exists, don't write in the headers when opened
	ofstream data;
	data.open (name, std::ios_base::app);
	if(empty) data << "vertices,radius, path_length, count\n";
	
//INITIALISE GRAPH
	//Make the graph and negative graph



	for(int system_num = 6; system_num<7; system_num++){
		int num_nodes, steps;
		float rad_init;
		num_nodes = 256*pow(2,system_num);
		steps =1;
		rad_init = 0.0000157;
		float x=1.0315; // x is radius multiplicative factor for log spacing
		float rad_curr = rad_init;

		std::random_device rd; 
		std::mt19937 gen(rd()); 
		std::uniform_real_distribution<> dis(0, 1);

		// make N x (D+1) vector for position of each node and final element is the nodes time sqrd

		vector< vector<float> > node_list (num_nodes,vector<float>(dimensions +1));
		for(int i=0; i<num_nodes; i++){
			for(int d=0; d<dimensions+1; d++){
				float pos_rand = dis(gen);
				node_list[i][d] = pos_rand;
			}
		}
		sort(node_list.begin(), node_list.end(),compare_time); // topological sort

		Graph g(num_nodes); 
		Graph neg_graph(num_nodes); // make graph for longest path
		for(int step = 0; step<steps; step++)
		{
			vector< vector <int> > short_path_results(0, vector<int>(2));
			vector <int> init_elem(2,0);
			short_path_results.push_back(init_elem);
			for(int running=0; running<3; running++)
			{
				for(int j=0; j<num_nodes; j++)
				{
					for(int k=0; k<j; k++)
					{
						float change_r2 = 0.0;
						for(int d=0; d<dimensions; d++)
						{
						 	change_r2 += (node_list[j][d]-node_list[k][d])*(node_list[j][d]-node_list[k][d]);
						}
						float change_tau = ((node_list[j][dimensions]-node_list[k][dimensions])*(node_list[j][dimensions]-node_list[k][dimensions])) - change_r2;
						if( step > 0){
							if(rad_curr - (rad_curr/x) < change_tau && change_tau < rad_curr)
							{
								graph_traits<Graph>::vertex_descriptor u,v;
						 		u=vertex(k,g);
						 		v=vertex(j,g);
						 		add_edge(u,v,1,g);
							 	add_edge(u,v,-1,neg_graph);
							}  	   
						}
						else{
							if(0 < change_tau && change_tau < rad_curr)
							{
								graph_traits<Graph>::vertex_descriptor u,v;
								u=vertex(k,g);
								v=vertex(j,g);
								add_edge(u,v,1,g);
								add_edge(u,v,-1,neg_graph);
							}
						}
					}
				}

		  		//Find shortest path matrix
		  		// Loop through shortest path matrix. For each connected pair, check if path
		  		//length is in vector. If is, add one to respective count, otherwise add new
		  		//element (sp,1)
			    for(int s =0; s<num_nodes; s++){
			    	property_map<Graph, vertex_distance_t>::type d_map = get(vertex_distance, g);
			    	dag_shortest_paths(g,s,distance_map(d_map));
			        graph_traits<Graph>::vertex_iterator vi, vi_end;
			        for(tie(vi, vi_end) = vertices(g); vi!=vi_end; ++vi){
			        	if(d_map[*vi] <num_nodes){
			            	//cout << s << ", " << *vi << ": " << d_map[*vi] << endl;
			            	bool not_in = true;
			            	for(int i=0; i<short_path_results.size(); i++){
			            		if(d_map[*vi] == short_path_results[i][0]){
			            			short_path_results[i][1]++;
			            			not_in = false;
			            			break;
			            		}
			            	} 
			            	if(not_in){
				            vector<int> new_elems = {d_map[*vi], 1};
				            cout << new_elems[0] << endl;
				            short_path_results.push_back(new_elems);
				            }
			            }
			        }
			    }
			    
		  		for(int l=0; l<short_path_results.size(); l++){
		  			data << num_nodes;
		  			data << ", ";
		  			data << rad_curr;
		  			data << ", ";
		  			data << short_path_results[l][0];
		  			data << ", ";
		  			data << short_path_results[l][1] << endl;
		  		}
		 



		  		rad_curr *= x;
			}
	  	}
	}  
  	data.close();
  	return 0;
}
