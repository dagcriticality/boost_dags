from subprocess import Popen, PIPE
import numpy as np
import matplotlib.pyplot as plt

"""
Open terminal in boost_dags and run g++ -o BOX_lsp -I /Users/robertanderson/Documents/boost_place/boost_1_65_1 1D_BOX_lsp.cpp 
"""

def longest_shortest_path(N,R):
    """Generate a 1D BOX RGG with Num_Vertices = N, Radius = R"""

    p = Popen(['./BOX_lsp'], shell=True, stdout=PIPE, stdin=PIPE)
    for i in [N,R]:
        param = str(i) + '\n'
        param = bytes(param, 'UTF-8')
        p.stdin.write(param)
        p.stdin.flush()
    LCC = p.stdout.readline().strip()
    return int(LCC.decode('UTF-8'))

N=100
radii = np.logspace(-5,-0.5,100)
runs = 20
results = []
for R in radii:
    count = 0.0
    for r in np.arange(runs):
        count += longest_shortest_path(N, R)
    results.append(count / runs)
plt.plot(radii, results)
plt.xlabel('R (N='+str(N)+')')
plt.ylabel('Size of the longest shortest path')
plt.show()