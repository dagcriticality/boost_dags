#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/range/irange.hpp>
#include <boost/pending/indirect_cmp.hpp>
#include <boost/graph/graph_traits.hpp>
#include <random>  
#include <boost/graph/dag_shortest_paths.hpp>
#include <boost/config.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/properties.hpp> 
#include <boost/graph/graph_concepts.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/overloading.hpp>
#include <boost/static_assert.hpp>
#include <boost/concept/assert.hpp>
#include <string>
using namespace std;
using namespace boost;


const int dimensions = 2;// dimensions = spacial dimensions = D 

typedef adjacency_list<vecS, vecS, bidirectionalS,property<vertex_distance_t, float>, 
property<edge_weight_t, float> > Graph;

bool compare_time(const vector<float>& a, const vector<float>& b){
        return a[dimensions] < b[dimensions];
        // function that compares the temporal ordering of two nodes
    }

void output_edges(Graph& g){
    graph_traits<Graph>::edge_iterator ei, ei_end;
    for (tie(ei, ei_end) = edges(g); ei != ei_end; ++ei){
      cout << *ei << endl;
    }
  }

template <class ComponentsMap>
class components_recorder : public dfs_visitor<>{
    typedef typename property_traits<ComponentsMap>::value_type comp_type;
public:
     components_recorder(ComponentsMap c, 
                      comp_type& c_count)
    : m_component(c), m_count(c_count) {}
  template <class Vertex, class Graph>
  void start_vertex(Vertex, Graph&) {
    if (m_count == (std::numeric_limits<comp_type>::max)())
      m_count = 0; // start counting components at zero
    else
      ++m_count;
  }
  template <class Vertex, class Graph>
  void discover_vertex(Vertex u, Graph&) {
    put(m_component, u, m_count);
  }
protected:
  ComponentsMap m_component;
  comp_type& m_count;
};

template <class Graph, class ComponentMap>
inline typename property_traits<ComponentMap>::value_type
connected_components(const Graph& g, ComponentMap c
                   BOOST_GRAPH_ENABLE_IF_MODELS_PARM(Graph, vertex_list_graph_tag))
{ 
if (num_vertices(g) == 0) return 0;

typedef typename graph_traits<Graph>::vertex_descriptor Vertex;
BOOST_CONCEPT_ASSERT(( WritablePropertyMapConcept<ComponentMap, Vertex> ));
// typedef typename boost::graph_traits<Graph>::directed_category directed;
// BOOST_STATIC_ASSERT((boost::is_same<directed, undirected_tag>::value));

typedef typename property_traits<ComponentMap>::value_type comp_type;
// c_count initialized to "nil" (with nil represented by (max)())
comp_type c_count((std::numeric_limits<comp_type>::max)());
components_recorder<ComponentMap> vis(c, c_count);
depth_first_search(g, visitor(vis));
return c_count + 1;
}

float longest_path(Graph& neg_graph, graph_traits<Graph>::vertex_descriptor s){

    property_map<Graph, vertex_distance_t>::type d_map = get(vertex_distance, neg_graph);
    vector<graph_traits<Graph>::vertex_descriptor> p(num_vertices(neg_graph));
    vector<graph_traits<Graph>::vertex_descriptor> d(num_vertices(neg_graph));
    dag_shortest_paths(neg_graph, s, distance_map(d_map));
    graph_traits<Graph>::vertex_iterator vi , vi_end;
    float longest = 0.0;
    for (tie(vi, vi_end) = vertices(neg_graph); vi != vi_end; ++vi){
        if (d_map[*vi] < longest){
          longest = d_map[*vi];
        }
    }
    return -longest;
}



int main(){

    ofstream myfile;
    myfile.open ("hypprobdata.csv");
    myfile << "vertices, prob, longest path, max component, longest shortest path, average shortest path \n";

    int num_nodes, steps;
    float prob;
    cin >> num_nodes >> prob >> steps;
    float curr_prob = prob;

    std::random_device rd; 
    std::mt19937 gen(rd()); 
    std::uniform_real_distribution<> dis(0, 1);

    // make N x (D+1) vector for position of each node and final element is the nodes time sqrd

    vector< vector<float> > node_list (num_nodes,vector<float>(dimensions +1)) ;
    for(int i=0; i<num_nodes; i++){
        float time = 0.0;
        for(int d=0; d<dimensions; d++){
            float pos_rand = dis(gen);
            time += pos_rand;
            node_list[i][d] = pos_rand;
        }
        node_list[i][dimensions]=time;
    }
    sort(node_list.begin(), node_list.end(),compare_time); // topological sort

    vector< pair<int, int> > poss_edges(0);
   for(int j=0; j<num_nodes; j++){
      for(int k=0; k<j; k++){
        bool within_range = true;
        float change_r2 = 0.0;
        for(int d=0; d<dimensions; d++){
          change_r2 += (node_list[j][d]-node_list[k][d])*(node_list[j][d]-node_list[k][d]);
        }
        float change_tau = ((node_list[j][dimensions]-node_list[k][dimensions])*(node_list[j][dimensions]-node_list[k][dimensions])) - change_r2;
        if(change_tau>0){
          poss_edges.push_back(make_pair(k,j));
        }
      }
    }

    int num_poss_edge = poss_edges.size();

    for(int step=0; step<steps; step++){
      Graph g(num_nodes);
      Graph neg_graph(num_nodes);
      for(int e=0; e< num_poss_edge; e++){
          float edge_prob = dis(gen);
          if(edge_prob < curr_prob){
            graph_traits<Graph>::vertex_descriptor u,v;
            int k = poss_edges[e].first;
            int j = poss_edges[e].second;
                u=vertex(k,g);
                v=vertex(j,g);
                add_edge(u,v,1,g);
                add_edge(u,v,-1,neg_graph);
          }
      }


      // calculate longest path and size of connected components
      vector<unsigned long> component(num_vertices(g));
      int comp_num = connected_components(g, make_iterator_property_map(component.begin(), get(vertex_index, g)));
      vector<unsigned long> comps(comp_num, 0); 
      for(unsigned long i = 0; i < num_vertices(g); i++)
      {
          comps[component[i]]++;
      }
      unsigned long maxcomp = *max_element(comps.begin(), comps.end());
      float lpath = longest_path(neg_graph, 0);
      for(int i = 1; i < comp_num; i++){
        int loc = find(component.begin(), component.end(), i) - component.begin();
        float next_lpath = longest_path(neg_graph, loc); 
        if(next_lpath > lpath){
          lpath = next_lpath;
        }
      }




      // calculate longest_shortest_path and average_shortest_path
      int lsp = 0;
      int cumul_sp = 0;
      int num_conn_nodes=0;
      for(int s =0; s<num_nodes; s++){
        property_map<Graph, vertex_distance_t>::type d_map = get(vertex_distance, g);
        dag_shortest_paths(g,s,distance_map(d_map));
        graph_traits<Graph>::vertex_iterator vi, vi_end;
        for(tie(vi, vi_end) = vertices(g); vi!=vi_end; ++vi){
          if(d_map[*vi] <num_nodes){
            // cout << s << ", " << *vi << ": " << d_map[*vi] << endl; 
            cumul_sp += d_map[*vi];
            num_conn_nodes += 1;
            if(d_map[*vi]>lsp){
              lsp = d_map[*vi];
            }
          }
        }
      }
      float avg_sp = float(cumul_sp)/float(num_conn_nodes);


      myfile << num_nodes;
      myfile << ",";
      myfile << curr_prob;
      myfile << ",";
      myfile << lpath;
      myfile << ",";
      myfile << maxcomp;
      myfile << ",";
      myfile << lsp;
      myfile << ",";
      myfile << avg_sp << endl;
      curr_prob += prob;

    }
    myfile.close();
    return 0;
}

