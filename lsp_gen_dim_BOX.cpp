#include <iostream>
#include <stdlib.h>
#include <boost/graph/adjacency_list.hpp>
#include <random>
#include <boost/graph/exterior_property.hpp>
#include <boost/graph/floyd_warshall_shortest.hpp>



using namespace std;
using namespace boost;

const int dimensions = 2;// dimensions = spacetime dimensions = D+1


// EdgeWeight stuff is just set so all edges can have weights = 1
typedef double t_weight;
typedef property<edge_weight_t, t_weight> EdgeWeightProperty;

typedef adjacency_list<vecS, vecS, directedS, no_property, EdgeWeightProperty> Graph;

typedef property_map<Graph, edge_weight_t>::type WeightMap;
typedef exterior_vertex_property<Graph, t_weight> DistanceProperty;
typedef DistanceProperty::matrix_type DistanceMatrix;
typedef DistanceProperty::matrix_map_type DistanceMatrixMap;

bool compare(const vector<float>& a, const vector<float>& b){
        return a[dimensions] < b[dimensions];
    }

int main(){

    int Num_Vert;
    float radius;
    cin >> Num_Vert >> radius;

    // make N x (D+1) vector for position of each node and final element is the nodes time sqrd

    vector< vector<float> > node_list (Num_Vert,vector<float>(dimensions +1)) ;
    for(int i=0; i<Num_Vert; i++){ // believe using indexing is quicker here than using iterators
        std::random_device rd; 
        std::mt19937 gen(rd()); 
        std::uniform_int_distribution<> dis(0, 1E5);
        float time_sqrd = 0.0;
        for(int d=0; d<dimensions; d++){
            float pos_rand = dis(gen)/1E5;
            time_sqrd += (pos_rand*pos_rand);
            node_list[i][d] = pos_rand;
        }
        node_list[i][dimensions]=time_sqrd;
    }

    // order vector in terms of time value

    sort(node_list.begin(), node_list.end(),compare);
    // add all vertices to graph and add edges if within radius

    Graph g;
    for(int j=0; j<Num_Vert; j++){
        Graph::vertex_descriptor v = add_vertex(g);
        for(int k=0; k<j; k++){
            bool within_range = true;
            for(int d=0; d<dimensions; d++){
                float dist=node_list[j][d]-node_list[k][d];
                if(abs(dist)>radius){
                    within_range = false;
                    break;
                }
            }
            if(within_range){
                graph_traits<Graph>::vertex_descriptor u;
                u=vertex(k,g);
                pair<adjacency_list<>::edge_descriptor, bool> q;
                q=add_edge(u,v,EdgeWeightProperty(1), g);
            }
            
        }
    }
    WeightMap weight_pmap = get(edge_weight, g);

    DistanceMatrix distances(num_vertices(g));
    DistanceMatrixMap dm(distances, g);


    bool valid = floyd_warshall_all_pairs_shortest_paths(g,dm, weight_map(weight_pmap));

    int longest_shortest_dist = 0;
    for (size_t i = 0; i<num_vertices(g); ++i){
        for(size_t j = 0; j<num_vertices(g); ++j){
            if(distances[i][j] != numeric_limits<t_weight>::max()){
                if(distances[i][j] > longest_shortest_dist){
                    longest_shortest_dist = distances[i][j];
                }
            }
        }
    }
    cout << longest_shortest_dist << endl;
    
    return 0;
}

