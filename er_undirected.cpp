#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/range/irange.hpp>
#include <boost/pending/indirect_cmp.hpp>
#include <boost/graph/graph_traits.hpp>
#include <random>  
#include <boost/graph/dag_shortest_paths.hpp>
#include <boost/config.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/properties.hpp> 
#include <boost/graph/graph_concepts.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/overloading.hpp>
#include <boost/static_assert.hpp>
#include <boost/concept/assert.hpp>
#include <string>
using namespace std;
using namespace boost;

const int dimensions = 2;

typedef adjacency_list<vecS, vecS, undirectedS,property<vertex_distance_t, float>, 
property<edge_weight_t, int> > Graph;

bool compare_time(const vector<float>& a, const vector<float>& b){
        return a[dimensions] < b[dimensions];
        // function that compares the temporal ordering of two nodes
    }

void output_edges(Graph& g){
    graph_traits<Graph>::edge_iterator ei, ei_end;
    for (tie(ei, ei_end) = edges(g); ei != ei_end; ++ei){
      cout << *ei << endl;
    }
  }

template <class ComponentsMap>
class components_recorder : public dfs_visitor<>{
    typedef typename property_traits<ComponentsMap>::value_type comp_type;
public:
     components_recorder(ComponentsMap c, 
                      comp_type& c_count)
    : m_component(c), m_count(c_count) {}
  template <class Vertex, class Graph>
  void start_vertex(Vertex, Graph&) {
    if (m_count == (std::numeric_limits<comp_type>::max)())
      m_count = 0; // start counting components at zero
    else
      ++m_count;
  }
  template <class Vertex, class Graph>
  void discover_vertex(Vertex u, Graph&) {
    put(m_component, u, m_count);
  }
protected:
  ComponentsMap m_component;
  comp_type& m_count;
};

template <class Graph, class ComponentMap>
inline typename property_traits<ComponentMap>::value_type
connected_components(const Graph& g, ComponentMap c
                   BOOST_GRAPH_ENABLE_IF_MODELS_PARM(Graph, vertex_list_graph_tag))
{ 
if (num_vertices(g) == 0) return 0;

typedef typename graph_traits<Graph>::vertex_descriptor Vertex;
BOOST_CONCEPT_ASSERT(( WritablePropertyMapConcept<ComponentMap, Vertex> ));
// typedef typename boost::graph_traits<Graph>::directed_category directed;
// BOOST_STATIC_ASSERT((boost::is_same<directed, undirected_tag>::value));

typedef typename property_traits<ComponentMap>::value_type comp_type;
// c_count initialized to "nil" (with nil represented by (max)())
comp_type c_count((std::numeric_limits<comp_type>::max)());
components_recorder<ComponentMap> vis(c, c_count);
depth_first_search(g, visitor(vis));
return c_count + 1;
}




int main(){

    ofstream myfile;
    myfile.open ("erdata_undir.csv");
    myfile << "vertices, edges, max component, longest shortest path, average shortest path, average degree \n";

    for(int running=0; running<1; running++)
    {
      for(int system_num=0; system_num<1; system_num++)
      {
        int num_nodes, steps, num_edges, edge_curr;
        float rad_init;
        num_nodes = 256*pow(2,system_num);
        steps =150;
        num_edges = ceil(num_nodes/20);
        float x = 1.041;
        float rad_curr = rad_init;

        std::random_device rd; 
        std::mt19937 gen(rd()); 
        std::uniform_real_distribution<> dis(0, 1);

        if(pow(x,(steps+1))*num_edges > num_nodes*(num_nodes-1)/2)
        {
          cout << "too many edges" << endl;
          return -1;
        }
        edge_curr = num_edges;

        Graph g(num_nodes); 
        Graph neg_graph(num_nodes); // make graph for longest path
        vector<pair<int,int>> edge_pool;
        for(int j=0; j<num_nodes; j++)
        {
          for(int k=0; k<j; k++)
          {
            pair<int,int> p{k,j};
            edge_pool.push_back(p);
          }
        }
        random_shuffle(edge_pool.begin(),edge_pool.end());
        for(int step = 0; step<steps; step++)
        {
          for(int edge_number=0; edge_number<num_edges; edge_number++)
          { 
            int u,v;
            tie(u,v) = edge_pool.back();
            edge_pool.pop_back();
            add_edge(u,v,1,g);
          }


          // calculate longest path and size of connected components
          vector<unsigned long> component(num_vertices(g));
          int comp_num = connected_components(g, make_iterator_property_map(component.begin(), get(vertex_index, g)));
          vector<unsigned long> comps(comp_num, 0); 
          for(unsigned long i = 0; i < num_vertices(g); i++)
          {
              comps[component[i]]++;
          }
          unsigned long maxcomp = *max_element(comps.begin(), comps.end());




          // calculate longest_shortest_path and average_shortest_path
          //int lsp = 0;
          //int cumul_sp = 0;
          //int num_conn_nodes=0;
          //vector<int> d(num_vertices(g));
          //for(int s =0; s<num_nodes; s++){
            //dijkstra_shortest_paths(g, s, predecessor_map(&p[0]).distance_map(&d[0]));
            //graph_traits<Graph>::vertex_iterator vi, vi_end;
            //for(tie(vi, vi_end) = vertices(g); vi!=vi_end; ++vi){
              //if(d[*vi] <num_nodes){
                //cout << s << ", " << *vi << ": " << d[*vi] << endl; 
                //cumul_sp += d[*vi];
                //num_conn_nodes += 1;
                //if(d[*vi]>lsp){
                  //lsp = d[*vi];
                //}
              //}
            //}
          //}
          //float avg_sp = float(cumul_sp)/float(num_conn_nodes);



          // sampling method lsp and asp
          int lsp_mean_method = 0;
          int cumul_sp_mean_meth = 0;
          int num_conn_nodes_mean_meth=0;
          int mean_meth_loops = 0;
          float averaging_criteria = 0.0001;
          int num_averaging_over = 10;
          property_map<Graph, edge_weight_t>::type weightmap = get(edge_weight,g);
          vector<graph_traits<Graph>::vertex_descriptor> p(num_vertices(g));
          vector<int> d(num_vertices(g));
          std::uniform_int_distribution<> rnd(0,num_nodes-1);
          vector<float> last_five_means (num_averaging_over,0);
          for(int i =0; i<num_averaging_over; i++){
            mean_meth_loops+=1;
            int s= rnd(gen); // note - I am choosing not to prevent same vertex being selected twice
            dijkstra_shortest_paths(g, s, predecessor_map(&p[0]).distance_map(&d[0]));
            graph_traits<Graph>::vertex_iterator vi, vi_end;
            for(tie(vi, vi_end) = vertices(g); vi!=vi_end; ++vi){
              if(d[*vi] <num_nodes){
                // cout << s << ", " << *vi << ": " << d_map[*vi] << endl; 
                cumul_sp_mean_meth+=d[*vi];
                num_conn_nodes_mean_meth += 1;
                if(d[*vi]>lsp_mean_method){
                  lsp_mean_method = d[*vi];
                }
              }
            }
            float tmp_avg = float(cumul_sp_mean_meth)/num_conn_nodes_mean_meth;
            last_five_means[i]=tmp_avg;
          }
          bool keep_mean_meth_loop_going = true;
          float tmp_avg = float(cumul_sp_mean_meth)/num_conn_nodes_mean_meth;
          float fluctuations = 0;
          for(int j =0; j<5; j++){
            fluctuations += pow((last_five_means[j]-tmp_avg),2)/pow(tmp_avg,2);
          }
          if(fluctuations<averaging_criteria){
            keep_mean_meth_loop_going = false;
          }
          while(keep_mean_meth_loop_going){
            mean_meth_loops+=1;
            int s= rnd(gen); // note - I am choosing not to prevent same vertex being selected twice
            dijkstra_shortest_paths(g, s, predecessor_map(&p[0]).distance_map(&d[0]));
            graph_traits<Graph>::vertex_iterator vi, vi_end;
            for(tie(vi, vi_end) = vertices(g); vi!=vi_end; ++vi){
              if(d[*vi] <num_nodes){
                cumul_sp_mean_meth+=d[*vi];
                num_conn_nodes_mean_meth += 1;
                cout << s << ", " << *vi << ": " << d[*vi] << endl;
                if(d[*vi]>lsp_mean_method){
                  lsp_mean_method = d[*vi];
                }
              }
            }
            for(int i = 0; i<num_averaging_over-1; i++){
              last_five_means[i]=last_five_means[i+1];
            }
            float tmp_avg = float(cumul_sp_mean_meth)/num_conn_nodes_mean_meth;
            last_five_means[num_averaging_over-1]= tmp_avg;
            float fluctuations = 0;
            for(int j =0; j<num_averaging_over; j++){
              fluctuations += pow((last_five_means[j]-tmp_avg),2)/pow(tmp_avg,2);
            }
            if(fluctuations<averaging_criteria){
              keep_mean_meth_loop_going = false;
            }
            if(mean_meth_loops==num_nodes){
              break;
            }
          }


          // calculate average degree
          float avg_deg = 0.0;
          for(int n =0; n<num_nodes; n++){
            avg_deg += float(degree(n,g))/num_nodes;
          }


          myfile << num_nodes;
          myfile << ",";
          myfile << edge_curr;
          myfile << ",";
          myfile << maxcomp;
          myfile << ",";
          myfile << lsp_mean_method;
          myfile << ",";
          myfile << last_five_means[num_averaging_over-1];
          myfile << ", ";
          myfile << avg_deg << endl;
          edge_curr += num_edges;
          num_edges = ceil(num_edges*x);
        }
      }
    }
    myfile.close();
    return 0;
}

