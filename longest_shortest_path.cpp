#include <iostream>
#include <stdlib.h>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/exterior_property.hpp>
#include <boost/graph/floyd_warshall_shortest.hpp>
#include <random>

using namespace std;
using namespace boost;

float Rad = 0.1;
int Num_Vert = 1000;

struct vertex_properties
    {
    float xpos;
    float ypos;
    };

typedef double t_weight;
typedef property<edge_weight_t, t_weight> EdgeWeightProperty;

typedef adjacency_list<vecS, vecS, directedS, vertex_properties, EdgeWeightProperty> Graph;
// look up vecS compared to linked list sort of thing

typedef property_map<Graph, edge_weight_t>::type WeightMap;
typedef exterior_vertex_property<Graph, t_weight> DistanceProperty;
typedef DistanceProperty::matrix_type DistanceMatrix;
typedef DistanceProperty::matrix_map_type DistanceMatrixMap;


bool test_pair_vertex(Graph::vertex_descriptor v1, Graph::vertex_descriptor v2, Graph g, float R){
    float distx = g[v1].xpos - g[v2].xpos;
    float disty = g[v1].ypos - g[v2].ypos;
    if(abs(distx)<R || abs(disty)<R){
        return true;
    }
    else{
        return false;
    }
}

bool temporal_order(Graph::vertex_descriptor v1, Graph::vertex_descriptor v2, Graph g){
    // returns true if v1 comes before v2, false otherwise
    float time1 = (g[v1].xpos*g[v1].xpos)+(g[v1].ypos*g[v1].ypos);
    float time2 = (g[v2].xpos*g[v2].xpos)+(g[v2].ypos*g[v2].ypos);
    if(time1<time2){
        return true;
    }
    else{
        return false;
    }

}

void add_a_node(Graph& g)
    {
    Graph::vertex_descriptor v = add_vertex(g);
    std::random_device rd; 
    std::mt19937 gen(rd()); 
    std::uniform_int_distribution<> dis(0, 1E5);
    float xr = dis(gen)/1E5;
    float yr = dis(gen)/1E5;
    g[v].xpos=xr;
    g[v].ypos=yr;
    typedef graph_traits<Graph>::vertex_iterator vertex_iter;
    std::pair<vertex_iter, vertex_iter> vp;
    for (vp = vertices(g); vp.first != vp.second-1; ++vp.first){
        // set condition to vp.second -1 to avoid self-loops
        if(test_pair_vertex(v,*vp.first, g, Rad)){
            // set Radius to be 0.5 for now
            if(temporal_order(v,*vp.first,g)){
                std::pair<adjacency_list<>::edge_descriptor, bool> p;
                p=add_edge(v, *vp.first,EdgeWeightProperty(1), g);
            }
            else{
                std::pair<adjacency_list<>::edge_descriptor, bool> q;
                q=add_edge(*vp.first,v,EdgeWeightProperty(1), g);
            }

        }
    }
    }


int main(){


    Graph g;

    std::random_device rd; 
    std::mt19937 gen(rd()); 
    std::uniform_int_distribution<> dis(0, 1E5);
    float xrand = dis(gen)/1E5;
    float yrand = dis(gen)/1E5;

    Graph::vertex_descriptor v1 = add_vertex(g);

    g[v1].xpos = xrand;
    g[v1].ypos = yrand;

    for(int i=0; i<Num_Vert; i++){ // here is where you define what N is to be
        add_a_node(g);
    }

    WeightMap weight_pmap = get(edge_weight, g);

    DistanceMatrix distances(num_vertices(g));
    DistanceMatrixMap dm(distances, g);


    bool valid = floyd_warshall_all_pairs_shortest_paths(g,dm, weight_map(weight_pmap));

    int longest_shortest_dist = 0;
    for (size_t i = 0; i<num_vertices(g); ++i){
        for(size_t j = 0; j<num_vertices(g); ++j){
            if(distances[i][j] != numeric_limits<t_weight>::max()){
                if(distances[i][j] > longest_shortest_dist){
                    longest_shortest_dist = distances[i][j];
                }
            }
        }
    }
    cout << "The longest shortest distance with N="<<Num_Vert<< " and R="<< Rad << " is " << longest_shortest_dist << endl;
    
    return 0;
}

